#ifndef FORM_LAYOUT_H
#define FORM_LAYOUT_H

#include <QDialog>

namespace Ui {
class form_layout;
}

class form_layout : public QDialog
{
    Q_OBJECT

public:
    explicit form_layout(QWidget *parent = 0);
    ~form_layout();

private:
    Ui::form_layout *ui;
};

#endif // FORM_LAYOUT_H
