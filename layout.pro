#-------------------------------------------------
#
# Project created by QtCreator 2016-08-30T14:52:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = layout
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    form_layout.cpp

HEADERS  += mainwindow.h \
    form_layout.h

FORMS    += mainwindow.ui \
    form_layout.ui
